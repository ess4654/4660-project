#include "Image.h"
#include "Converter.h"

#include <fstream>

using namespace cv;
using namespace std;

Image::Image()
{
}

Image::~Image()
{
}

void Image::CreateSpreadsheet(Mat img)
{
    int RGB[3];
    int H, S, V;

    ofstream fout;
    fout.open("data.txt");

    for(int k = 0; k < img.rows; k++)
    {
        for(int j = 0; j < img.cols; j++)
        {
            //output coordinates
            fout<<k<<" "<<j<<" ";

            //output RGB Values
            for(int i = 2; i > -1; i--)
            {
                RGB [i] = static_cast<int>(img.at<Vec3b>(cv::Point(j, k)).val[i]);
                fout<<RGB[i]<<" ";
            }

            //Convert RGB colors to HSV and add them to the file
            Converter().RGBtoHSV(RGB[2], RGB[1], RGB[0], H, S, V);
            fout<<H<<" "<<S<<" "<<V<<endl;
        }
    }
    fout.close();
}

/***** BELOW ARE ALL SET/GET FUNCTIONS *****/

double Image::HSV(double X)
{
    if(X == 0) return imgData.HSV;
    this->imgData.HSV = X;
    return X;
}

double Image::HSL(double X)
{
    if(X == 0) return imgData.HSL;
    this->imgData.HSL = X;
    return X;
}

double Image::EarthMoversDistance(double X)
{
    if(X == 0) return imgData.EarthMoversDistance;
    this->imgData.EarthMoversDistance = X;
    return X;
}

double Image::HueD(double X)
{
    if(X == 0) return imgData.HueDescriptor;
    this->imgData.HueDescriptor = X;
    return X;
}

double Image::HueM(double X)
{
    if(X == 0) return imgData.HueModel;
    this->imgData.HueModel = X;
    return X;
}

double Image::Brightness(double X)
{
    if(X == 0) return imgData.Brightness;
    this->imgData.Brightness = X;
    return X;
}

double Image::Edge(double X)
{
    if(X == 0) return imgData.Edge;
    this->imgData.Edge = X;
    return X;
}

double Image::Texture(double X)
{
    if(X == 0) return imgData.Texture;
    this->imgData.Texture = X;
    return X;
}

double Image::Entropy(double X)
{
    if(X == 0) return imgData.Entropy;
    this->imgData.Entropy = X;
    return X;
}

double Image::HSVWave(double X)
{
    if(X == 0) return imgData.HSVWave;
    this->imgData.HSVWave = X;
    return X;
}

double Image::AvgHSV(double X)
{
    if(X == 0) return imgData.AvgHSV;
    this->imgData.AvgHSV = X;
    return X;
}

double Image::SF(double X)
{
    if(X == 0) return imgData.SegmentFive;
    this->imgData.SegmentFive = X;
    return X;
}

double Image::Focus(double X)
{
    if(X == 0) return imgData.Focus;
    this->imgData.Focus = X;
    return X;
}

double Image::Centroids(double X)
{
    if(X == 0) return imgData.Centroids;
    this->imgData.Centroids = X;
    return X;
}

double Image::HSF(double X)
{
    if(X == 0) return imgData.HSegmentFive;
    this->imgData.HSegmentFive = X;
    return X;
}

double Image::SSF(double X)
{
    if(X == 0) return imgData.SSegmentFive;
    this->imgData.SSegmentFive = X;
    return X;
}

double Image::VSF(double X)
{
    if(X == 0) return imgData.VSegmentFive;
    this->imgData.VSegmentFive = X;
    return X;
}

double Image::BST(double X)
{
    if(X == 0) return imgData.BrightnessSegmentThree;
    this->imgData.BrightnessSegmentThree = X;
    return X;
}

double Image::CMSF(double X)
{
    if(X == 0) return imgData.ColorModel;
    this->imgData.ColorModel = X;
    return X;
}

double Image::Coordinates(double X)
{
    if(X == 0) return imgData.Coordinates;
    this->imgData.Coordinates = X;
    return X;
}

double Image::MassVariance(double X)
{
    if(X == 0) return imgData.MassVarience;
    this->imgData.MassVarience = X;
    return X;
}

double Image::Skewness(double X)
{
    if(X == 0) return imgData.Skewness;
    this->imgData.Skewness = X;
    return X;
}

double Image::Contrast(double X)
{
    if(X == 0) return imgData.Contrast;
    this->imgData.Contrast = X;
    return X;
}

double Image::DOF(double X)
{
    if(X == 0) return imgData.DOF;
    this->imgData.DOF = X;
    return X;
}

double Image::Total(double X)
{
    if(X == 0) return imgData.total;
    this->imgData.total = X;
    return X;
}

Image::ImageValues Image::ImgData()
{
    return this->imgData;
}

#include "Calculator.h"

#include <cmath>
#include <fstream>
#include <iostream>
#include <algorithm>

char* dataFile = "data.txt";

using namespace cv;
using namespace std;

Calculator::Calculator()
{
}

Calculator::Calculator(Mat img)
{
    this->rows = img.rows;
    this->cols = img.cols;
    this->dataFields = rows * cols;

    this->FromPicture = new data*[rows];
    for(int i = 0; i < rows; ++i)
        FromPicture[i] = new data[cols];

    ifstream fin;
    fin.open("data.txt");

    for(int j = 0; j < rows; j++){
    for(int i = 0; i < cols; i++)
    {
        fin >> this->FromPicture[j][i].y
            >> this->FromPicture[j][i].x
            >> this->FromPicture[j][i].R
            >> this->FromPicture[j][i].G
            >> this->FromPicture[j][i].B
            >> this->FromPicture[j][i].H
            >> this->FromPicture[j][i].S
            >> this->FromPicture[j][i].V;
    }}

    fin.close();

    int H = 0, S = 0, V = 0;

    for(int y = 0; y < rows; y++){
    for(int z = 0; z < cols; z++)
    {
        H += FromPicture[y][z].H;
        S += FromPicture[y][z].S;
        V += FromPicture[y][z].V;
    }}

    myAverage.H = H/dataFields;
    myAverage.S = S/dataFields;
    myAverage.V = V/dataFields;
}

Calculator::~Calculator()
{
    delete *FromPicture;
    delete FromPicture;
    FromPicture = nullptr;
}

void Calculator::Run(Image* WorkingImage, Mat image)
{
    WorkingImage->HSV(CalculateHSV(image));
    cout<<"HSV Score: "<<WorkingImage->HSV()<<endl<<endl<<endl;

    WorkingImage->HSL(CalculateHSL(image));
    cout<<"HSL Score: "<<WorkingImage->HSL()<<endl<<endl<<endl;

    WorkingImage->EarthMoversDistance(CalculateEMD(image));
    cout<<"Color Score (EMD): "<<WorkingImage->EarthMoversDistance()<<endl<<endl<<endl;

    WorkingImage->HueD(CalculateHueDescriptors());
    cout<<"Hue Descriptors: "<<WorkingImage->HueD()<<endl<<endl<<endl;

    WorkingImage->HueM(CalculateHueModels(image));
    cout<<"Hue Model: "<<WorkingImage->HueM()<<endl<<endl<<endl;

    WorkingImage->Brightness(CalculateBrightness(image));
    cout<<"Image Brightness: "<<WorkingImage->Brightness()<<endl<<endl<<endl;

    WorkingImage->Edge(CalculateEdge(image));
    cout<<"Edge Score: "<<WorkingImage->Edge()<<endl<<endl<<endl;

    WorkingImage->Texture(CalculateTexture(image));
    cout<<"Texture Score: "<<WorkingImage->Texture()<<endl<<endl<<endl;

    WorkingImage->Entropy(CalculateRGBEntropy(image));
    cout<<"RGB Entropy Score: "<<WorkingImage->Entropy()<<endl<<endl<<endl;

    WorkingImage->HSVWave(CalculateHSVWavelet());
    cout<<"HSV Wave Score: "<<WorkingImage->HSVWave()<<endl<<endl<<endl;

    WorkingImage->AvgHSV(CalculateAverageHSV(image));
    cout<<"Average HSV Score: "<<WorkingImage->AvgHSV()<<endl<<endl<<endl;

    WorkingImage->SF(CalculateLargest5());
    cout<<"5 Segments Score: "<<WorkingImage->SF()<<endl<<endl<<endl;

    WorkingImage->Focus(CalculateHSLFocus());
    cout<<"Focus Score: "<<WorkingImage->Focus()<<endl<<endl<<endl;

    WorkingImage->Centroids(CalculateCentroids());
    cout<<"Centroids Score: "<<WorkingImage->Centroids()<<endl<<endl<<endl;

    WorkingImage->HSF(CalculateHue5());
    cout<<"Hue Segment 5 Score: "<<WorkingImage->HSF()<<endl<<endl<<endl;

    WorkingImage->SSF(CalculateSat5());
    cout<<"Saturation Segment 5 Score: "<<WorkingImage->SSF()<<endl<<endl<<endl;

    WorkingImage->VSF(CalculateVal5());
    cout<<"Value Segment 5 Score: "<<WorkingImage->VSF()<<endl<<endl<<endl;

    WorkingImage->BST(Calculate3Brightness());
    cout<<"Brightness Segment 3 Score: "<<WorkingImage->BST()<<endl<<endl<<endl;

    WorkingImage->CMSF(CalculateColorModel5());
    cout<<"Color Model Segment 5: "<<WorkingImage->CMSF()<<endl<<endl<<endl;

    WorkingImage->Coordinates(CalculateCoordinates());
    cout<<"Coordinates Value: "<<WorkingImage->Coordinates()<<endl<<endl<<endl;

    WorkingImage->Contrast(CalculateContrast());
    cout<<"Contrast Value: "<<WorkingImage->Contrast()<<endl<<endl<<endl;

    WorkingImage->MassVariance(CalculateMassVariance());
    cout<<"Mass Variance Value: "<<WorkingImage->MassVariance()<<endl<<endl<<endl;

    WorkingImage->Skewness(CalculateSkewness());
    cout<<"Skewness Score: "<<WorkingImage->Skewness()<<endl<<endl<<endl;


    WorkingImage->DOF(CalculateDOF(image));
    cout<<"Depth of Field Score: "<<WorkingImage->DOF()<<endl<<endl<<endl;

    WorkingImage->Total(WorkingImage->DOF() +
                        WorkingImage->AvgHSV() +
                        WorkingImage->Entropy() +
                        WorkingImage->HSV() +
                        WorkingImage->Edge() +
                        WorkingImage->Texture() +
                        WorkingImage->EarthMoversDistance());

    cout<<"Total Score: "<<WorkingImage->Total()<<endl<<endl<<endl;
}

double Calculator::CalculateHSV(Mat img)
{
	data** FromPicture = new data*[img.rows];
	for (int i = 0; i < img.rows; ++i)
		FromPicture[i] = new data[img.cols];

    ifstream fin;
    fin.open("data.txt");

    for(int j = 0; j < rows; j++){
    for(int i = 0; i < cols; i++)
    {
        fin >> FromPicture[j][i].y
            >> FromPicture[j][i].x
            >> FromPicture[j][i].R
            >> FromPicture[j][i].G
            >> FromPicture[j][i].B
            >> FromPicture[j][i].H
            >> FromPicture[j][i].S
            >> FromPicture[j][i].V;
    }}

    fin.close();

    Average myAverage;
    int H = 0, S = 0, V = 0;

    for(int y = 0; y < img.rows; y++){
    for(int z = 0; z < img.cols; z++)
    {
        H += FromPicture[y][z].H;
        S += FromPicture[y][z].S;
        V += FromPicture[y][z].V;
    }}

    myAverage.H = H/dataFields;
    myAverage.S = S/dataFields;
    myAverage.V = V/dataFields;

    cout<<"Average HSV For Entire Image: "<<myAverage.H<<" "<<myAverage.S<<" "<<myAverage.V<<endl;

    double deltaH = 0, deltaS = 0, deltaV = 0;
    for(int a = 0; a < img.rows; a++){
    for(int b = 0; b < img.cols; b++)
    {
        deltaH += sin(abs(FromPicture[a][b].H - myAverage.H)/115.0);
        deltaS += abs(sin(abs(FromPicture[a][b].S - myAverage.S)/40.0));
        deltaV += sin(abs(FromPicture[a][b].V - myAverage.V)/162.0);
    }}

    deltaH /= dataFields;
    deltaS /= dataFields;
    deltaV /= dataFields;

    cout<<deltaH<<" "<<deltaS<<" "<<deltaV<<endl;

	delete *FromPicture;
	delete FromPicture;

    return (deltaH + deltaS + deltaV);
}

double Calculator::CalculateHSL(cv::Mat)
{
    return 0;
}

void Calculator::GetBin64(const int R, const int G, const int B, int &binX, int &binY, int &binZ)
{
	int L, U, V;
	Convert.rgb2luv(R, G, B, L, U, V);

	binX = (10 * L) / 251;
	binY = ((U + 124) * 10)/ 861;
	binZ = ((V + 140) * 10)/ 641;
	binX %= 4;
	binY %= 4;
	binZ %= 4;
}

double Calculator::dist3D(int i, int x, int j, int y, int k, int z)
{
	int dx, dy, dz;

	dx = i - x;
	dy = j - y;
	dz = k - z;

	//Formula for the distance between points in 3D space
	double dist = pow((dx*dx + dy*dy + dz*dz), 0.5);

	return dist;
}

double Calculator::EMD(const int bin64[4][4][4])
{
	double RunningSum = 0;
	int count = 0;
	int a, b, c;
	int i, j, k;
	int max;
	int radius;

	for(int z = 0; z<4; z++)
	{
		for(int y = 0; y<4; y++)
		{
			for(int x = 0; x<4; x++)
			{
				if(bin64[y][x][z] == 0) continue; //If the bin has no value ignore it

				count += bin64[y][x][z];

				max = 0;
				radius = 1;
				a = b = c = i = j = k = 0;

				//loop will run multiple times if their is no neighbor within 1 block
				while(1)
				{
					//This loop will find the neighbor with the maximum
					//value at the closest distance to the location
					for(int u = 0; u<27; u++)
					{
						//Set the coordinates of the neighbor to check
						a = radius * (u % 3) - 1;
						b = radius * ((-u % 3) + (u % 9)) / 3 - 1;
						c = radius * (u / 9) - 1;

						//If the coordinates are the current spot skip the loop
						if(a == 0 && b == 0 && c == 0) continue;
						if(x+a < 0 || x+a > 3 || y+b < 0 || y+b > 3 || z+c < 0 || z+c > 3) continue; //If out of bounds, then this neighbor does not exist. Continue.

						//If the number at the neighbor is larger than the current max,
						//then upate the max, and the location of the neighbor
						if(max < bin64[y + b][x + a][z + c])
						{
							max = bin64[y + b][x + a][z + c];
							i = a;
							j = b;
							k = c;
						}
					}
					//If Max has been set otherwise increase the looking distance
					if(max > 0) break;
					radius++;
					if(radius == 3) break;
				}

				if(max <= 0)
				{
					a = i = x;
					b = j = y;
					c = k = z;
				}

				//cout<<"Max at ("<<x<<","<<y<<","<<z<<"): "<<max<<endl;
				//The sum is the most amount of earth you can move over a certain distance
				RunningSum += min(max, bin64[y][x][z]) * dist3D(i, x, j, y, k, z);
			}
		}
	}

	RunningSum /= count;

	cout<<"Total Count = "<<count<<endl;

	//This will prevent taking the square roon of a negative number
	if(RunningSum > 5.19) return 3;
	if(RunningSum < 1) return 0;

	return (10.89 * pow(RunningSum - 1, 0.25))/pow(27.0, 0.5);
}

double Calculator::CalculateEMD(Mat img)
{
    int trash;
    int R, G, B, binY, binX, binZ;
	int bin64[4][4][4] = {0};

    ifstream fin;
    fin.open("data.txt");

    while(1)
	{
		fin>>trash>>trash>>R>>G>>B>>trash>>trash>>trash;

		if(fin.eof()) break;

		GetBin64(R, G, B, binX, binY, binZ);
		bin64[binY][binX][binZ]++;
	}

    fin.close();

    int count = 0;

	for(int z = 0; z<4; z++)
	{
		//cout<<"Z: "<<z+1<<endl;
		for(int y = 0; y<4; y++)
		{
			for(int x = 0; x<4; x++)
			{
				//cout<<bin64[y][x][z]<<" ";
				count += bin64[y][x][z];
			}
			//cout<<endl;
		}
		//cout<<endl;
	}

    return EMD(bin64);
}

double Calculator::PresentHue(int bin[], bool present[])
{
    double count = 0;

    for(int j = 0; j<20; j++)
    {
        present[j] = (bin[j] > 0.1 * dataFields) || (bin[j] < 0.01 * dataFields);
        if(present[j]) count++;
    }

    return count/20;
}

double Calculator::MissingHue(bool present[])
{
    double pres = 0, miss = 0;

    for(int j = 0; j<20; j++)
    {
        if(present[j]) pres++;
        else miss++;
    }

    return ((20-miss)/(21-pres))/20;
}

double Calculator::FreqentHue()
{
    return 0;
}

double Calculator::ArcMissing()
{
    return 0;
}

double Calculator::ArcPresent()
{
    return 0;
}

double Calculator::PercentPixels(int bin[])
{
    double count = 0;

    for(int i = 0; i < 20; i++)
        if(count < bin[i]) count = bin[i];

    cout<<"Pixels in Largest Bin: "<<(count/dataFields)*100<<"%\n";
    return count/dataFields;
}

double Calculator::InsignificantHues(int bin[])
{
    double count = 0;
    for(int i = 0; i<20; i++)
        if(bin[i] > 0.05 * dataFields) count++;

    cout<<"Distribution: "<<(20-count)/20<<endl;
    return (20-count)/20;
}

double Calculator::CalculateHueDescriptors()
{return 0;
    double v1, v2, v3, v4, v5, v6, v7;
    int histBin[20] = {0};
    bool present[20] = {false};
    int pos;

    for(int j = 0; j < rows; j++){
    for(int i = 0; i < cols; i++){
        pos = FromPicture[j][i].H/18%20;
        if(pos >= 20 || pos < 0) continue;
        histBin[pos]++;
    }}

    v1 = PresentHue(histBin, present);
    v2 = MissingHue(present);
    v3 = 0;//FreqentHue();
    v4 = 0;//ArcMissing();
    v5 = 0;//ArcPresent();
    v6 = InsignificantHues(histBin);
    v7 = PercentPixels(histBin);
    return (v1 + v2 + v3 + v4 + v5 + v6 + v7);
}

double Calculator::CalculateHueModels(cv::Mat)
{
    return 0;
}

double Calculator::CalculateBrightness(cv::Mat)
{
    return 0.0;
}

double Calculator::SobelsMethod()
{
    return 0;
}

double Calculator::BoundedEdge(double SD, double Area, const long long HorizontalSum[], const long long VerticleSum[], int rows, int cols)
{
    double f;
    int BoundedArea;
    int left = 0, right = 0, top = 0, bottom = 0;
    int lower, upper, greater, less;
    int count;

    long long hSD = 0, vSD = 0;
    long long AvgH = 0, AvgV = 0;

    for(int x = 0; x < rows; x++)
    {
        AvgH += HorizontalSum[x];
    }
    AvgH /= rows;
    for(int x = 0; x < rows; x++)
    {
        hSD += pow((HorizontalSum[x]-AvgH),2);
    }
    hSD /= rows - 1;
    hSD = sqrt(hSD);

    for(int y = 0; y < cols; y++)
    {
        AvgV += VerticleSum[y];
    }
    AvgV /= cols;
    for(int y = 0; y < cols; y++)
    {
        vSD += pow((VerticleSum[y]-AvgV),2);
    }
    vSD /= cols - 1;
    vSD = sqrt(vSD);

    less = AvgH - (SD * hSD);
    greater = AvgH + (SD * hSD);
    upper = AvgV + (SD * vSD);
    lower = AvgV - (SD * vSD);

    count = 0;
    for(int t = 0; t<rows; t++)
    {
        if(count == 5) break;
        if(less < HorizontalSum[t]) left = t;
        else if(less >= HorizontalSum[t]) count++;
    }

    count = 0;
    for(int t = rows-1; t>=0; t--)
    {
        if(count == 5) break;
        if(greater > HorizontalSum[t]) right = t;
        else if(greater <= HorizontalSum[t]) count++;
    }

    count = 0;
    for(int t = 0; t<cols; t++)
    {
        if(count == 5) break;
        if(lower < VerticleSum[t]) bottom = t;
        else if(lower >= VerticleSum[t]) count++;
    }

    count = 0;
    for(int t = cols-1; t>=0; t--)
    {
        if(count == 5) break;
        if(upper > VerticleSum[t]) top = t;
        else if(upper <= VerticleSum[t]) count++;
    }

    BoundedArea = abs(left-right) * abs(top-bottom);
    f = (static_cast<double>(BoundedArea)/static_cast<double>(Area));

    return f;
}

double Calculator::CalculateEdge(Mat img)
{
    Mat imgLaplacian;
    int Area;
    double f1, f2, f3;
    double SD81 = 1.281552;
    double SD96 = 2.0;

    // ok, now try different kernel
    Mat kernel = (Mat_<float>(3,3) <<
        1,  1, 1,
        1, -8, 1,
        1,  1, 1);
        // another approximation of second derivate, more stronger

    // do the laplacian filtering as it is
    // well, we need to convert everything in something more deeper then CV_8U
    // because the kernel has some negative values,
    // and we can expect in general to have a Laplacian image with negative values
    // BUT a 8bits unsigned int (the one we are working with) can contain values from 0 to 255
    // so the possible negative number will be truncated
    filter2D(img, imgLaplacian, CV_32F, kernel);
    img.convertTo(img, CV_8U);

    int RGBSum;
    int RGB[3];
	
	long long* VerticleSum = new long long [imgLaplacian.cols];
	long long* HorizontalSum = new long long[imgLaplacian.rows];
    for(int j = 0; j < imgLaplacian.cols; j++)
        VerticleSum[j] = 0;
    for(int i = 0; i < imgLaplacian.rows; i++)
        HorizontalSum[i] = 0;

    for(int k = 0; k < imgLaplacian.rows; k++){
    for(int j = 0; j < imgLaplacian.cols; j++){
        RGBSum = 0;
        for(int i = 2; i > -1; i--)
        {
            RGB[i] = imgLaplacian.at<Vec3b>(cv::Point(j, k)).val[i];
            RGBSum += RGB[i]/100;
        }
        if(HorizontalSum[k] < pow(2,63)) HorizontalSum[k] += RGBSum;
        if(VerticleSum[j] < pow(2,63)) VerticleSum[j] += RGBSum;
    }}

    Area = imgLaplacian.cols * imgLaplacian.rows;
    f1 = BoundedEdge(SD81, Area, HorizontalSum, VerticleSum, imgLaplacian.rows, imgLaplacian.cols);
    f2 = BoundedEdge(SD96, Area, HorizontalSum, VerticleSum, imgLaplacian.rows, imgLaplacian.cols);
    f3 = SobelsMethod();


	delete HorizontalSum;
	delete VerticleSum;
	HorizontalSum = nullptr;
	VerticleSum = nullptr;

    return f1 + f2 + f3;
}

double Calculator::CalculateTexture(Mat img)
{
	data** FromPicture = new data*[img.rows];
	for (int i = 0; i < img.rows; ++i)
		FromPicture[i] = new data[img.cols];

    ifstream fin;
    fin.open(dataFile);

    for(int j = 0; j < rows; j++){
    for(int i = 0; i < cols; i++)
    {
        fin >> FromPicture[j][i].y
            >> FromPicture[j][i].x
            >> FromPicture[j][i].R
            >> FromPicture[j][i].G
            >> FromPicture[j][i].B
            >> FromPicture[j][i].H
            >> FromPicture[j][i].S
            >> FromPicture[j][i].V;
    }}

    fin.close();

    double H = 0, S = 0, V = 0;
    int HBox[9], SBox[9], VBox[9];
    int Cmax, Cmin, delta;

    for (int n = 0; n < rows; n++){
    for(int m = 0; m < cols; m++){
        for(int z = 0; z < 9; z++)
        {
            HBox[z] = FromPicture[abs(n + ((z/3) - 1)) % rows][abs(m + ((z%3) - 1)) % cols].H;
            SBox[z] = FromPicture[abs(n + ((z/3) - 1)) % rows][abs(m + ((z%3) - 1)) % cols].S;
            VBox[z] = FromPicture[abs(n + ((z/3) - 1)) % rows][abs(m + ((z%3) - 1)) % cols].V;
        }
        Cmax = max(max(max(max(max(max(max(max(HBox[0],HBox[1]),HBox[2]),HBox[3]),HBox[4]),HBox[5]),HBox[6]),HBox[7]),HBox[8]);
        Cmin = min(min(min(min(min(min(min(min(HBox[0],HBox[1]),HBox[2]),HBox[3]),HBox[4]),HBox[5]),HBox[6]),HBox[7]),HBox[8]);
        delta = abs(Cmax - Cmin);
        H += delta;

        Cmax = max(max(max(max(max(max(max(max(SBox[0],SBox[1]),SBox[2]),SBox[3]),SBox[4]),SBox[5]),SBox[6]),SBox[7]),SBox[8]);
        Cmin = min(min(min(min(min(min(min(min(SBox[0],SBox[1]),SBox[2]),SBox[3]),SBox[4]),SBox[5]),SBox[6]),SBox[7]),SBox[8]);
        delta = abs(Cmax - Cmin);
        S += delta;

        Cmax = max(max(max(max(max(max(max(max(VBox[0],VBox[1]),VBox[2]),VBox[3]),VBox[4]),VBox[5]),VBox[6]),VBox[7]),VBox[8]);
        Cmin = min(min(min(min(min(min(min(min(VBox[0],VBox[1]),VBox[2]),VBox[3]),VBox[4]),VBox[5]),VBox[6]),VBox[7]),VBox[8]);
        delta = abs(Cmax - Cmin);
        V += delta;
    }}

    H /= dataFields;
    V /= dataFields;
    S /= dataFields;

    H = (-H/100.0) + 1;
    H = (H < 0) ? 0:H;
    S = (-S/150.0) + 1;
    S = (S < 0) ? 0:S;
    V = (-V/150.0) + 1;
    V = (V < 0) ? 0:V;

    cout<<"Average 9 Point Box Values"<<endl;
    cout<<H<<" "<<S<<" "<<V<<endl;

	for (int i = 0; i < img.cols; ++i) {
		delete[] FromPicture[i];
	}
	delete FromPicture;

    return 2.0 * ((H + S + V) / 3.0);
}

double Calculator::CalculateRGBEntropy(Mat img)
{
    double Val;
    if (img.channels()==3) cvtColor(img, img, CV_BGR2GRAY);
    /// Establish the number of bins
    int histSize = 256;
    /// Set the ranges ( for B,G,R) )
    float range[] = { 0, 256 } ;
    const float* histRange = { range };
    bool uniform = true; bool accumulate = false;
    Mat hist;
    /// Compute the histograms:
    calcHist( &img, 1, 0, Mat(), hist, 1, &histSize, &histRange, uniform, accumulate );
    hist /= img.total();

    Mat logP;
    cv::log(hist,logP);

    float entropy = -1*sum(hist.mul(logP)).val[0];

    /*
        This value is meant to represent the graph that will give high values for
        anything between 3 and 5 with ~4.8 being the max value, and also including
        a sharp drop off anything higher then a 5. No lower than 0 though.
    */
    Val = ((-pow(entropy, 9.0)) * log10((0.0988 * entropy) + 0.465))/12350.0;
    Val = (Val <= 0) ? 0:Val;
    return Val;
}

double Calculator::CalculateHSVWavelet()
{
    return 0;
}

double Calculator::CalculateAverageHSV(Mat img)
{
	data** FromPicture = new data*[img.rows];
	for (int i = 0; i < img.rows; ++i)
		FromPicture[i] = new data[img.cols];
    data Bit9[9];
    int counter = 0;

    ifstream fin;
    fin.open(dataFile);

    for(int j = 0; j < img.rows; j++){
    for(int i = 0; i < img.cols; i++)
    {
        fin >> FromPicture[j][i].y
            >> FromPicture[j][i].x
            >> FromPicture[j][i].R
            >> FromPicture[j][i].G
            >> FromPicture[j][i].B
            >> FromPicture[j][i].H
            >> FromPicture[j][i].S
            >> FromPicture[j][i].V;
    }}

    fin.close();

    Average myAverage;
    long long R, G, B;
    int H, S, V;

    for (int n = 0; n < 3; n++){
    for(int m = 0; m < 3; m++){

        R = 0;
        G = 0;
        B = 0;
        H = 0;
        S = 0;
        V = 0;

        for(int l = (n * img.rows/3); l < img.rows/3 + (n * img.rows/3); l++){
        for(int k = (m * img.cols/3); k < img.cols/3 + (m * img.cols/3); k++)
        {
            R += FromPicture[l][k].R;
            G += FromPicture[l][k].G;
            B += FromPicture[l][k].B;
        }}

        R /= dataFields/9;
        G /= dataFields/9;
        B /= dataFields/9;

        myAverage.R = R;
        myAverage.G = G;
        myAverage.B = B;

        Convert.RGBtoHSV(myAverage.R, myAverage.G, myAverage.B, H, S, V);

        myAverage.H = H;
        myAverage.S = S;
        myAverage.V = V;

        //cout<<myAverage.H<<" "<<myAverage.S<<" "<<myAverage.V<<endl;
        Bit9[counter].H = myAverage.H;
        Bit9[counter].S = myAverage.S;
        Bit9[counter].V = myAverage.V;
        counter++;
    }}

    H = 0;
    S = 0;
    V = 0;

    for(int y = 0; y < 9; y++)
    {
        H += Bit9[y].H;
        S += Bit9[y].S;
        V += Bit9[y].V;
    }

    H /= 9;
    S /= 9;
    V /= 9;
    cout<<"Average HSV For 9 Point Quadrant: "<<H<<" "<<S<<" "<<V<<endl;

    double deltaH = 0, deltaS = 0, deltaV = 0;
    for(int z = 0; z < 9; z++)
    {
        deltaH += sin(abs(Bit9[z].H - H)/115.0);
        deltaS += abs(sin(abs(Bit9[z].S - S)/40.0));
        deltaV += sin(abs(Bit9[z].V - V)/162.0);
    }

    deltaH /= 9.0;
    deltaS /= 9.0;
    deltaV /= 9.0;

    cout<<deltaH<<" "<<deltaS<<" "<<deltaV<<endl;

	for (int i = 0; i < img.cols; ++i) {
		delete[] FromPicture[i];
	}
	delete FromPicture;

    return (deltaH + deltaS + deltaV);
}

double Calculator::CalculateLargest5()
{
    return 0;
}

double Calculator::CalculateHSLFocus()
{
    return 0;
}

double Calculator::CalculateCentroids()
{
    return 0;
}

double Calculator::CalculateHue5()
{
    return 0;
}

double Calculator::CalculateSat5()
{
    return 0;
}

double Calculator::CalculateVal5()
{
    return 0;
}

double Calculator::Calculate3Brightness()
{
    return 0;
}

double Calculator::CalculateColorModel5()
{
    return 0;
}

double Calculator::CalculateCoordinates()
{
    return 0;
}

double Calculator::CalculateContrast()
{
    return 0;
}

double Calculator::CalculateMassVariance()
{
    return 0;
}

double Calculator::CalculateSkewness()
{
    return 0;
}

//This function will calculate the Depth of field for any given image. Max(3 points)
double Calculator::CalculateDOF(Mat img)
{
	data** FromPicture = new data*[img.rows];
	for (int i = 0; i < img.rows; ++i)
		FromPicture[i] = new data[img.cols];
    data Bit16[16];
    int counter = 0;

    ifstream fin;
    fin.open(dataFile);

    for(int j = 0; j < img.rows; j++){
    for(int i = 0; i < img.cols; i++)
    {
        fin >> FromPicture[j][i].y
            >> FromPicture[j][i].x
            >> FromPicture[j][i].R
            >> FromPicture[j][i].G
            >> FromPicture[j][i].B
            >> FromPicture[j][i].H
            >> FromPicture[j][i].S
            >> FromPicture[j][i].V;
    }}

    fin.close();

    Average myAverage;
    long long R, G, B;
    int H, S, V;

    for (int n = 0; n < 4; n++){
    for(int m = 0; m < 4; m++){

        R = 0;
        G = 0;
        B = 0;
        H = 0;
        S = 0;
        V = 0;

        for(int l = (n * rows/4); l < rows/4 + (n * rows/4); l++){
        for(int k = (m * cols/4); k < cols/4 + (m * cols/4); k++)
        {
            R += FromPicture[l][k].R;
            G += FromPicture[l][k].G;
            B += FromPicture[l][k].B;
        }}

        R /= dataFields/16;
        G /= dataFields/16;
        B /= dataFields/16;

        myAverage.R = R;
        myAverage.G = G;
        myAverage.B = B;

        Convert.RGBtoHSV(myAverage.R, myAverage.G, myAverage.B, H, S, V);

        myAverage.H = H;
        myAverage.S = S;
        myAverage.V = V;

        //cout<<myAverage.H<<" "<<myAverage.S<<" "<<myAverage.V<<endl;
        Bit16[counter].R = myAverage.R;
        Bit16[counter].G = myAverage.G;
        Bit16[counter].B = myAverage.B;
        counter++;
    }}

    int H1, H2, S1, S2, V1, V2;

    R = 0;
    G = 0;
    B = 0;

    for(int y = 0; y < 16; y++)
    {
        R += Bit16[y].R;
        G += Bit16[y].G;
        B += Bit16[y].B;
    }

    R /= 16;
    G /= 16;
    B /= 16;
    Convert.RGBtoHSV(R, G, B, H, S, V);
    H1 = H;
    S1 = S;
    V1 = V;
    cout<<"Outer: "<<H<<" "<<S<<" "<<V<<endl;

    R = 0;
    G = 0;
    B = 0;
    int temp[4] = {5,6,9,10};
    for(int z = 0; z < 4; z++)
    {
        R += Bit16[temp[z]].R;
        G += Bit16[temp[z]].G;
        B += Bit16[temp[z]].B;
    }

    R /= 4;
    G /= 4;
    B /= 4;
    Convert.RGBtoHSV(R, G, B, H, S, V);
    H2 = H;
    S2 = S;
    V2 = V;
    cout<<"Inner: "<<H<<" "<<S<<" "<<V<<endl;

    cout<<sin(abs(H1 - H2)/115.0)<<" "<<abs(sin(abs(S1 - S2)/40.0))<<" "<<sin(abs(V1 - V2)/162.0)<<endl;

	for (int i = 0; i < img.cols; ++i) {
		delete[] FromPicture[i];
	}
	delete FromPicture;

    return (sin(abs(H1 - H2)/115.0) + abs(sin(abs(S1 - S2)/40.0)) + sin(abs(V1 - V2)/162.0));
}

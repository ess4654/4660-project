
#include "Converter.h"
#include "Image.h"

#include <algorithm>

using namespace cv;
using namespace std;

Converter::Converter()
{
}

void Converter::RGBtoHSV(int R, int G, int B, int& H, int& S, int& V)
{
    int Cmax, Cmin;
    int delta;

    //Calculate needed values
    Cmax = max(max(R, G), B);
    Cmin = min(min(R, G), B);
    delta = Cmax - Cmin;

    //output H value
    if(delta == 0) H = 0;
    else if(Cmax == R) H = 60 * fmod(((G - B)/static_cast<double>(delta)), 6);
    else if(Cmax == G) H = 60 * ((B - R)/static_cast<double>(delta) + 2);
    else if(Cmax == B) H = 60 * ((R - G)/static_cast<double>(delta) + 4);
    if(H < 0) H += 360;

    //output S value
    S = (Cmax == 0) ? 0:((static_cast<double>(delta) / Cmax) * 255.0) + 1;

    //Output the value of V
    V = Cmax;

    H = (H > 359) ? 0:H;
    S = (S > 255) ? 255:S;
    V = (V > 255) ? 255:V;
}

void Converter::rgb2xyz(const int R, const int G, const int B, double &X, double &Y, double &Z)
{
	double var_R = ( R / 255.0 );
	double var_G = ( G / 255.0 );
	double var_B = ( B / 255.0 );

	if ( var_R > 0.04045 ) var_R = pow(( ( var_R + 0.055 ) / 1.055 ) , 2.4);
	else                   var_R /= 12.92;
	if ( var_G > 0.04045 ) var_G = pow(( ( var_G + 0.055 ) / 1.055 ) , 2.4);
	else                   var_G /= 12.92;
	if ( var_B > 0.04045 ) var_B = pow(( ( var_B + 0.055 ) / 1.055 ) , 2.4);
	else                   var_B /= 12.92;

	var_R *= 100;
	var_G *= 100;
	var_B *= 100;

	X = var_R * 0.4124 + var_G * 0.3576 + var_B * 0.1805;
	Y = var_R * 0.2126 + var_G * 0.7152 + var_B * 0.0722;
	Z = var_R * 0.0193 + var_G * 0.1192 + var_B * 0.9505;
}

void Converter::rgb2luv(const int R, const int G, const int B, int &L, int &U, int &V)
{
	double X, Y, Z;
	rgb2xyz(R, G, B, X, Y, Z);

	double xn, yn, un, vn, u, v;
	xn = 0.312713;
	yn = 0.329016;
	un = 4 * xn / (-2 * xn + 12 * yn + 3);
 	vn = 9 * yn / (-2 * xn + 12 * yn + 3);
 	u = 4 * X / (X + 15 * Y + 3 * Z);
 	v = 9 * Y / (X + 15 * Y + 3 * Z);

	L = 116 * pow((Y/100.0),(1.0/3.0)) - 16;
 	U = 13 * L * (u - un);
 	V = 13 * L * (v - vn);

	L %= 101;
	U %= 221;
	V %= 117;
}

//This function will return a decimal value given it's string format
double Converter::string_to_decimal(string num)
{
	int pos;
	double n;

	pos = num.find(".", 0);
	n = stoi(num.substr(0, pos));

	num = num.substr(pos + 1, num.size()-pos - 1);
	if(num.size() > 9)
		num = num.substr(0, 9);
	n += stoi(num) / pow(10.0, num.size());

	return n;
}

string Converter::my_to_string(double decimal)
{
	string str = "";
	string dec = to_string(decimal);
	int back = to_string(decimal).size();
	int pos;

	str += to_string(static_cast<int>(decimal));

	for(int i = dec.size()-1; i>= 0; i--)
	{
		if(dec[i] == '0')
			back = i;
		else
			break;
	}

	if(back-1 == str.size()) return str;

	pos = dec.find(".", 0);
	str += dec.substr(pos, back - pos);

	return str;
}

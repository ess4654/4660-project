#ifdef __linux__
	#include <glob.h>
#elif _WIN32 || _WIN64
	#include <filesystem>
	namespace fs = std::experimental::filesystem;
	#define fixedImread(filename, isColor) imread(filename.c_str(), isColor)
#endif

#include <fstream>
#include <cmath>

#include "FileManager.h"

using namespace std;

FileManager::FileManager()
{

}

vector<string> FileManager::setOutputFiles(vector<string> files, const string sub, const string rep)
{
	int pos;
	for(int i = 0; i<files.size(); i++)
	{
		pos = files[i].find(sub);
		files[i] = rep + files[i].substr(pos + sub.size(), files[i].size() - sub.size());
	}
	return files;
}

vector<string> FileManager::getFiles(char* dir)
{
	vector<string> files;

	#ifdef __linux__
		glob_t glob_result;

		glob(dir,GLOB_TILDE,NULL,&glob_result);
		for(unsigned int i = 0; i < glob_result.gl_pathc; ++i)
			files.push_back(glob_result.gl_pathv[i]);
	#elif _WIN32 || _WIN64
		for (auto & f : fs::directory_iterator("./temp/")) {
			files.push_back(f.path().string());
		}
	#endif

    return files;
}

//This will change a single data field in the JSON given it's identifier
void FileManager::changeDataField(const string field, const double TO, string &data)
{
	int pos, pos2;
	pos = data.find(field, 0);
	pos += field.size() + 2;

	pos2 = data.find("\"", pos);
	data.insert(pos, Convert.my_to_string(TO));
}

//For changing any string values in the JSON
void FileManager::changeDataField(const string field, const string TO, string &data)
{
	int pos, pos2;
	pos = data.find(field, 0);
	pos += field.size() + 2;

	pos2 = data.find("\"", pos);
	data = data.substr(0, pos) + data.substr(pos2, data.size()-pos2);
	data.insert(pos, TO);
}

//This will return the value in the data attached with a specific key word.
double FileManager::findValue(string pic, const string Value)
{
	int pos, pos2;
	double n;
	string num;

	pos = pic.find(Value, 0);
	pos += Value.size()+2;
	pos2 = pic.find("\"", pos);
	num = pic.substr(pos, pos2-pos);

	return Convert.string_to_decimal(num);
}

int FileManager::FindPlace(const vector<string> &pictures, const string data, const double Total)
{
	int place = 0;
	int mid;
	double n;

	//Note because this is a sorted list, a binary search could be used
	//To find the right location, but for the essence of project time,
	//I've just gone with a simple linear search.
	for(int i = 0; i<pictures.size(); i++)
	{
		n = findValue(pictures[i], "\"Total\":");
		if(Total <= n)
			place = i+1;
	}

	return place;
}

string FileManager::buildImageReference(string &data, const Image::ImageValues &Val)
{
	string reference;

	reference = "<table width=100%><tr align=left><th>Total Score: " + Convert.my_to_string(Val.total) + "<br> <br> </th> </tr>" +
				"<tr align=left> <th>" +
				"Hue Saturation Value Distribution: " + Convert.my_to_string(Val.HSV) + "<br>" +
				"Hue Saturation Lightness Score: " + Convert.my_to_string(Val.HSL) + "<br>" +
				"Image Colorfulness: " + Convert.my_to_string(Val.EarthMoversDistance) + "<br>" +
				"Hue Descriptors: " + Convert.my_to_string(Val.HueDescriptor) + "<br>" +
				"Hue Models: " + Convert.my_to_string(Val.HueModel) + "<br>" +
				"Image Bightness: " + Convert.my_to_string(Val.Brightness) + "<br>" +
				"Edge Energy Score: " + Convert.my_to_string(Val.Edge) + "<br>" +
				"Image Texture: " + Convert.my_to_string(Val.Texture) + "<br>" +
				"Entropy of Pixel Distribution: " + Convert.my_to_string(Val.Entropy) + "<br>" +
				"HSV Image Smoothness: " + Convert.my_to_string(Val.HSVWave) + "<br>" +
				"The Rule of Thirds: " + Convert.my_to_string(Val.AvgHSV) + "<br>" +
				"Largest 5 Segments Value: " + Convert.my_to_string(Val.SegmentFive) + "<br>" +
				"</th> <th>" +
				"Focus Score: " + Convert.my_to_string(Val.Focus) + "<br>" +
				"Image Centroids: " + Convert.my_to_string(Val.Centroids) + "<br>" +
				"5 Segments Hue: " + Convert.my_to_string(Val.HSegmentFive) + "<br>" +
				"5 Segments Saturation: " + Convert.my_to_string(Val.SSegmentFive) + "<br>" +
				"5 Segments Value: " + Convert.my_to_string(Val.VSegmentFive) + "<br>" +
				"3 Segments Image Brightness: " + Convert.my_to_string(Val.BrightnessSegmentThree) + "<br>" +
				"5 Segments Color Model: " + Convert.my_to_string(Val.ColorModel) + "<br>" +
				"3 Segments Pixel Coordinates: " + Convert.my_to_string(Val.Coordinates) + "<br>" +
				"3 Segments Mass Variance: " + Convert.my_to_string(Val.MassVarience) + "<br>" +
				"3 Segments Skewness: " + Convert.my_to_string(Val.Skewness) + "<br>" +
				"Image Contrast: " + Convert.my_to_string(Val.Contrast) + "<br>" +
				"Depth of Field Score: " + Convert.my_to_string(Val.DOF) + "<br>" +
				"</th> </tr> </table>";

	return reference;
}

void FileManager::UpdateDataFields(string &data, const Image::ImageValues &Val, const string name)
{
	changeDataField("\"name\":", name, data);

	changeDataField("\"HSV\":", Val.HSV, data);
	changeDataField("\"HSL\":", Val.HSL, data);
	changeDataField("\"Colorfulness\":", Val.EarthMoversDistance, data);
	changeDataField("\"HueD\":", Val.HueDescriptor, data);
	changeDataField("\"HueM\":", Val.HueModel, data);
	changeDataField("\"Brightness\":", Val.Brightness, data);
	changeDataField("\"Edge\":", Val.Edge, data);
	changeDataField("\"Texture\":", Val.Texture, data);
	changeDataField("\"Entropy\":", Val.Entropy, data);
	changeDataField("\"HSVWave\":", Val.HSVWave, data);
	changeDataField("\"AvgHSV\":", Val.AvgHSV, data);
	changeDataField("\"SegmentFive\":", Val.SegmentFive, data);
	changeDataField("\"Focus\":", Val.Focus, data);
	changeDataField("\"Centroids\":", Val.Centroids, data);
	changeDataField("\"HueSegmentFive\":", Val.HSegmentFive, data);
	changeDataField("\"SaturationSegmentFive\":", Val.SSegmentFive, data);
	changeDataField("\"ValueSegmentFive\":", Val.VSegmentFive, data);
	changeDataField("\"BrightnessSegmentThree\":", Val.BrightnessSegmentThree, data);
	changeDataField("\"ColorModelSegmentFive\":", Val.ColorModel, data);
	changeDataField("\"Coordinates\":", Val.Coordinates, data);
	changeDataField("\"MassVariance\":", Val.MassVarience, data);
	changeDataField("\"Skewness\":", Val.Skewness, data);
	changeDataField("\"Contrast\":", Val.Contrast, data);
	changeDataField("\"LowDepthOfField\":", Val.DOF, data);
	changeDataField("\"Total\":", Val.total, data);

	changeDataField("\"reference\":", buildImageReference(data, Val), data);
}

void FileManager::UpdateJSON(vector<string> &pictures, const Image::ImageValues &vals, const string ImgName)
{
	ifstream fin;
	string temp;
	string data = "";
	int pos;

	//Grab the data template from the JSON
	fin.open("./img_data_template.json");
	while(getline(fin, temp))
		data += (temp+"\n");
	fin.close();

	UpdateDataFields(data, vals, ImgName);

	//Find the location based on the Total value to insert the data
	pos = FindPlace(pictures, data, vals.total);
	data += ("\n" + Convert.my_to_string(pos));

	//Update the data into the vector
	data = data.substr(0, data.size()-3);
	data.insert(1, Convert.my_to_string(pos));
	data = data.substr(1, data.size()-Convert.my_to_string(pos).size());
	pictures.insert(pictures.begin()+pos, data);

	//This will update all valued after the newly inserted one to have the correct
	//placement number
	int place;
	for(int i = pos+1; i<pictures.size(); i++)
	{
		place = 0;
		while(pictures[i][place] != ':') place++;

		pictures[i] = pictures[i].substr(place, pictures[i].size()-place);
		pictures[i] = Convert.my_to_string(i) + pictures[i];
	}
}

void FileManager::SaveJson(const char* location, vector<string> pictures, const string top, const string bottom)
{
	ofstream fout;

	pictures[pictures.size()-1] = (pictures[pictures.size()-1].substr(0, pictures[pictures.size()-1].size()-1));

	fout.open(location);

	fout<<top<<endl;

    if(!CLEAR_JSON) {
        for(auto X: pictures)
            fout<<X<<endl;
    }
	fout<<bottom;

	fout.close();
}

vector<string> FileManager::GetJSON(const char* location, string &top, string &bottom)
{
	ifstream fin;
	string data, temp;
	vector<string> Img_Data;

	data = "";
	bottom = "}";

	fin.open(location);

	getline(fin,top);
	while(getline(fin, temp))
	{
		if(temp[0] >= 49 && temp[0] <= 57)
		{
			Img_Data.push_back(data.substr(0, data.size()-1));
			data = "";
		}
		data += (temp + "\n");
	}

	if(data.size() <= 10) return Img_Data;

	data = data.substr(0, data.size()-3);
	data += ",";

	Img_Data.push_back(data);
	fin.close();

	return Img_Data;
}

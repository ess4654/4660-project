#ifndef IMAGE_H
#define IMAGE_H

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/opencv.hpp>
#include <opencv/cv.h>

class Image
{
    public:
        struct ImageValues
        {
            double HSV, HSL, EarthMoversDistance, HueDescriptor, HueModel,
                   Brightness, Edge, Texture, Entropy, HSVWave,
                   AvgHSV, SegmentFive, Focus, Centroids, HSegmentFive,
                   SSegmentFive, VSegmentFive, BrightnessSegmentThree,
                   ColorModel, Coordinates, MassVarience, Skewness,
                   Contrast, DOF;
            double total;
        };

    private:
        ImageValues imgData;

    public:
        Image();
        virtual ~Image();
        void CreateSpreadsheet(cv::Mat img);

        /***** BELOW ARE ALL SET/GET FUNCTIONS *****/
        double HSV(double X = 0);
        double HSL(double X = 0);
        double EarthMoversDistance(double X = 0);
        double HueD(double X = 0);
        double HueM(double X = 0);
        double Brightness(double X = 0);
        double Edge(double X = 0);
        double Texture(double X = 0);
        double Entropy(double X = 0);
        double HSVWave(double X = 0);
        double AvgHSV(double X = 0);
        double SF(double X = 0);
        double Focus(double X = 0);
        double Centroids(double X = 0);
        double HSF(double X = 0);
        double SSF(double X = 0);
        double VSF(double X = 0);
        double BST(double X = 0);
        double CMSF(double X = 0);
        double Coordinates(double X = 0);
        double MassVariance(double X = 0);
        double Skewness(double X = 0);
        double Contrast(double X = 0);
        double DOF(double X = 0);
        double Total(double X = 0);
        ImageValues ImgData();
};

#endif

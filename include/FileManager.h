#ifndef FILEMANAGER_H
#define FILEMANAGER_H
#define FALSE 0
#define TRUE 1

/******** This Flag will Reset the Data in the JSON *******/
#define CLEAR_JSON FALSE

#include <vector>
#include <string>
#include "Image.h"
#include "Converter.h"
#include "RemoveFiles.h"

class FileManager
{
    private:
        Converter Convert;
        void changeDataField(const std::string, const double, std::string &);
        void changeDataField(const std::string, const std::string, std::string &);
        double findValue(std::string pic, const std::string Value);
        int FindPlace(const std::vector<std::string> &, const std::string, const double);
        std::string buildImageReference(std::string &, const Image::ImageValues &);
        void UpdateDataFields(std::string &, const Image::ImageValues &, const std::string);

    public:
        FileManager();
        std::vector<std::string> setOutputFiles(std::vector<std::string>, const std::string, const std::string);
        std::vector<std::string> getFiles(char*);

        void UpdateJSON(std::vector<std::string> &, const Image::ImageValues &, const std::string);
        void SaveJson(const char*, std::vector<std::string>, const std::string, const std::string);
        std::vector<std::string> GetJSON(const char*, std::string &, std::string &);
        void rem(const char* C) {RemoveFiles().rem(C);}
};

#endif

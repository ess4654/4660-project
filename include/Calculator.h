#ifndef CALCULATOR_H
#define CALCULATOR_H

#include "Converter.h"
#include "Image.h"

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/opencv.hpp>
#include <opencv/cv.h>

class Calculator
{
    private:
        Converter Convert;

        void GetBin64(const int, const int, const int, int&, int&, int&);
        double dist3D(int, int, int, int, int, int);
        double EMD(const int[4][4][4]);
        double PresentHue(int bin[], bool present[]);
        double MissingHue(bool present[]);
        double FreqentHue();
        double ArcMissing();
        double ArcPresent();
        double PercentPixels(int bin[]);
        double InsignificantHues(int bin[]);
        double SobelsMethod();
        double BoundedEdge(double, double Area, const long long[], const long long[], int, int);

        struct data
        {
            int x, y, R, G, B, H, S, V;
        };

        struct Average
        {
            int R, G, B, H, S, V;
        };

        data** FromPicture;
        Average myAverage;
        int dataFields;
        int rows, cols;

        //All the Calculation Functions
        double CalculateHSV(cv::Mat);
        double CalculateHSL(cv::Mat);
        double CalculateEMD(cv::Mat img);
        double CalculateHueDescriptors();
        double CalculateHueModels(cv::Mat);
        double CalculateBrightness(cv::Mat);
        double CalculateEdge(cv::Mat);
        double CalculateTexture(cv::Mat);
        double CalculateRGBEntropy(cv::Mat);
        double CalculateHSVWavelet();
        double CalculateAverageHSV(cv::Mat);
        double CalculateLargest5();
        double CalculateHSLFocus();
        double CalculateCentroids();
        double CalculateHue5();
        double CalculateSat5();
        double CalculateVal5();
        double Calculate3Brightness();
        double CalculateColorModel5();
        double CalculateCoordinates();
        double CalculateContrast();
        double CalculateMassVariance();
        double CalculateSkewness();
        double CalculateDOF(cv::Mat);

    public:
        Calculator();
        virtual ~Calculator();
        Calculator(cv::Mat);
        void Run(Image*, cv::Mat);
};

#endif

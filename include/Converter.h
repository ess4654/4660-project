#ifndef CONVERTER_H
#define CONVERTER_H

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/opencv.hpp>
#include <opencv/cv.h>
#include <string>

class Converter
{
    public:
        Converter();
        double string_to_decimal(std::string num);
        std::string my_to_string(double decimal);
        //Convert RGB to HSV
        void RGBtoHSV(int, int, int, int&, int&, int&);
        void rgb2xyz(const int, const int, const int, double&, double&, double&);
        void rgb2luv(const int, const int, const int, int&, int&, int&);
};

#endif

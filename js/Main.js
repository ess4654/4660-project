$(window).load(function(){
	$(document).ready(function(){
		Update();
		loadGrid();
	});
});

var page_index = 0;
var MaxOnPage = 12;
var count;

function fileupload()
{   alert("Upload Failed");
	var file_data = $("#avatar").prop("files")[0];   // Getting the properties of file from file field
	var form_data = new FormData();                  // Creating object of FormData class
	form_data.append("file", file_data)              // Appending parameter named file with properties of file_field to form_data
	form_data.append("user_id", 123)                 // Adding extra parameters to form_data
	$.ajax({
                url: "/",
                dataType: 'script',
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,                         // Setting the data attribute of ajax with file_data
                type: 'post'
       })
}

// Helper function that formats the file sizes
function formatFileSize(bytes) {
    if (typeof bytes !== 'number') {
        return '';
    }

    if (bytes >= 1000000000) {
        return (bytes / 1000000000).toFixed(2) + ' GB';
    }

    if (bytes >= 1000000) {
        return (bytes / 1000000).toFixed(2) + ' MB';
    }

    return (bytes / 1000).toFixed(2) + ' KB';
}

function Update()
{
	document.getElementById('page-number').innerHTML = "Page " + (page_index + 1);
}

function TransitPage(N)
{
	if(N == -1 && page_index == 0 || N == 1 && page_index == count/MaxOnPage)
		return;
	page_index += N;
	Update();
	loadGrid();
}

function loadGrid()
{
	count = Object.keys(PictureDir).length;
	var info;
	var backVisibility = page_index == 0 ? "hidden":"visible";
	var forwardVisibility = (count >= MaxOnPage * (page_index-1) + MaxOnPage)&&(count <= MaxOnPage * page_index + MaxOnPage) ? "hidden":"visible";

	document.getElementById("back-button").style.visibility = backVisibility;
	document.getElementById("forward-button").style.visibility = forwardVisibility;
	
	for(i = page_index * MaxOnPage; i < count && i < MaxOnPage*(page_index+1); i++)
	{
		info = (PictureDir[i%count].name + "<br>" + PictureDir[i%count].reference);

		if(i%3 == 0 && i != page_index * MaxOnPage)
		{
			document.getElementById('pictures' + i%(MaxOnPage-1)).innerHTML = "</div><div class='w3-row w3-grayscale-min'>";
		}
		if(i == page_index * MaxOnPage)
		{
			document.getElementById('pictures').innerHTML = "<div class='w3-row w3-grayscale-min'>" 
												+ CallGrid('./photo_grid.html')
												+ PictureDir[i%count].name
												+ CallGrid('./photo_grid_close.html')
												+ info
												+ "\"></div><div id='pictures" + ((i+1)%(MaxOnPage-1)) +"'></div>";
		} else {
			document.getElementById('pictures' + i%(MaxOnPage-1)).innerHTML = CallGrid('./photo_grid.html')
												+ PictureDir[i%count].name
												+ CallGrid('./photo_grid_close.html')
												+ info
												+ "\"></div><div id='pictures" + ((i+1)%(MaxOnPage-1)) +"'></div>";
		}
	}
}

function CallGrid(href)
{	
	var xmlhttp = new XMLHttpRequest();

	xmlhttp.open("GET", href, false);
	xmlhttp.send();

	return xmlhttp.responseText;
}
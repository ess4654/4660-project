#include "Converter.h"
#include "Calculator.h"
#include "Image.h"
#include "FileManager.h"

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/opencv.hpp>
#include <opencv/cv.h>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <sys/types.h>
#include <vector>

/***** Specify the maximum size of image the buffer can handle *****/
#define MAX_D 525

using namespace cv;
using namespace std;

int main()
{
	FileManager DataManager;
	Calculator* Calculate;
	vector<string> IPics, OPics;
	vector<string> JSON;
	string topLine, bottomLine;
	
    IPics = DataManager.getFiles("./temp/*"); 
    OPics = DataManager.setOutputFiles(IPics, "./temp/", "./Images/");

    double factor;

    Image* WorkingImage;

    if(IPics.size() == 0)
    {
        if(CLEAR_JSON)
            DataManager.SaveJson("./img_data.json", JSON, "var PictureDir = {", "}");
        return 1;
    }

    for(int i = 0; i < IPics.size(); i++)
    {
        cout<<"Image Name: "<<IPics[i]<<endl<<endl;
        Mat image = imread(IPics[i].c_str(), CV_LOAD_IMAGE_COLOR);

        if(image.empty())
        {
            if(CLEAR_JSON)
                DataManager.SaveJson("./img_data.json", JSON, "var PictureDir = {", "}");
            return -1;
        }

        if (image.cols * image.rows > (MAX_D * MAX_D))
        {
            factor = (image.cols > image.rows) ? ((MAX_D - 25.0) / image.cols):((MAX_D - 25.0) / image.rows);
            resize(image, image, cv::Size(), factor, factor);
        }

        WorkingImage = new Image();
        Calculate = new Calculator(image);

        WorkingImage->CreateSpreadsheet(image);

        Calculate->Run(WorkingImage, image);

        JSON = DataManager.GetJSON("./img_data.json", topLine, bottomLine);
        DataManager.UpdateJSON(JSON, WorkingImage->ImgData(), OPics[i].substr(9, OPics[i].size() - 9));
        DataManager.SaveJson("./img_data.json", JSON, topLine, bottomLine);

        delete WorkingImage;
        delete Calculate;
        WorkingImage = nullptr;
        Calculate = nullptr;
    }

    for(int k = 0; k < OPics.size(); k++)
    {
        Mat image = imread(IPics[k], CV_LOAD_IMAGE_COLOR);
        if(image.empty())
           return -1;

        imwrite(OPics[k], image);
    }
    for(int j = 0; j < IPics.size(); j++)
        DataManager.rem(IPics[j].c_str());

    waitKey(0);
    return 0;
}
